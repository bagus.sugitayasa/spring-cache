/**
 * 
 */
package id.training.telkomsigma.springcache.springcachedemo.hazelcastconfig;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.hazelcast.config.Config;
import com.hazelcast.config.EvictionPolicy;
import com.hazelcast.config.MapConfig;
import com.hazelcast.config.MaxSizeConfig;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version Id: 
 */
@Configuration
public class HazelcastConfig {
	@Bean
	public Config haselcastConfig() {
		return new Config()
				.setInstanceName("hazelcast-instance")
				.addMapConfig(new MapConfig()
						.setName("products")
						.setMaxSizeConfig(
								new MaxSizeConfig(
										200, 
										MaxSizeConfig.MaxSizePolicy.FREE_HEAP_SIZE
										)
						)
						.setEvictionPolicy(EvictionPolicy.LRU)
						.setTimeToLiveSeconds(20)
				);
						
	}
}
