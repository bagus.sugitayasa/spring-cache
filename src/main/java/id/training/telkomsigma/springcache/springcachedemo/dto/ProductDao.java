/**
 * 
 */
package id.training.telkomsigma.springcache.springcachedemo.dto;

import org.springframework.data.repository.PagingAndSortingRepository;

import id.training.telkomsigma.springcache.springcachedemo.entity.Product;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version Id: 
 */
public interface ProductDao extends PagingAndSortingRepository<Product, String>{
	public Product findByName(String name);
	public Product findByCode(String code);
}
