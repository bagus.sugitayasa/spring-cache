/**
 * 
 */
package id.training.telkomsigma.springcache.springcachedemo.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import id.training.telkomsigma.springcache.springcachedemo.dto.ProductDao;
import id.training.telkomsigma.springcache.springcachedemo.entity.Product;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version Id: 
 */
@Service
@CacheConfig(cacheNames = "products")
public class ProductService {
	private static final Logger LOG = LoggerFactory.getLogger(ProductService.class);
	@Autowired private ProductDao productDao;
	
	@Cacheable(key = "#name")//, condition = "#name=='Product 002'")
	public Product findProductByName(String name) {
		LOG.info("### Call service product by name {}", name);
		return productDao.findByName(name);
	}
	
	@Cacheable(key = "#code")//, unless = "#result.name=='Product p004'")
	public Product findProductByCode(String code) {
		LOG.info("### Call service product by code {}", code);
		return productDao.findByCode(code);
	}
	
	@CachePut(key = "#name")
	public Product updateProduct(String id, String name) {
		LOG.info("### Call service put product id {}, name {}", id, name);
		Product p = productDao.findOne(id);
		p.setName(name);
		productDao.save(p);
		return p;
	}
	
	@CacheEvict(key = "#name")
	public void deleteProduct(String name) {
		LOG.info("### Call service evict product name {}", name);
	}
}
