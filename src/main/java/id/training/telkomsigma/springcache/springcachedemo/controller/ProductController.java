/**
 * 
 */
package id.training.telkomsigma.springcache.springcachedemo.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.training.telkomsigma.springcache.springcachedemo.entity.Product;
import id.training.telkomsigma.springcache.springcachedemo.service.ProductService;


/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version Id: 
 */
@RestController @RequestMapping("/spring-cache/product")
public class ProductController {
	private static final Logger LOG = LoggerFactory.getLogger(ProductController.class);
	@Autowired private ProductService productService;
	@Autowired private CacheManager cacheManager;
	
	@GetMapping("/getByName/{name}")
	public Product getDataProductByName(@PathVariable String name) {
		LOG.info("## product controller getByName called here!!");
		LOG.info("PROVIDER YANG DIGUNAKAN : {}",cacheManager.getClass().getName());
		return productService.findProductByName(name);
	}
	
	@GetMapping("/getByCode/{code}")
	public Product getDataProductByCode(@PathVariable String code) {
		LOG.info("## product controller getByCode called here!!");
		return productService.findProductByCode(code);
	}
	
	@GetMapping("/updateProduct/{id}/{name}")
	public Product updateDataProduct(@PathVariable String id, @PathVariable String name) {
		LOG.info("## product controller put product called here!!");
		return productService.updateProduct(id, name); 
	}
	
	@GetMapping("/deleteProduct/name/{name}")
	public String deleteDataProduct(@PathVariable String name) {
		LOG.info("## product controller delete product called here!!");
		productService.deleteProduct(name);
		return "Delete Product Cache berhasil !!!";
	}
}