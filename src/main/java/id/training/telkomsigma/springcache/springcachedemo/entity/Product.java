/**
 * 
 */
package id.training.telkomsigma.springcache.springcachedemo.entity;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version Id: 
 */
@Data @Entity @Table
public class Product {
	@Id @GeneratedValue(generator="uuid") @GenericGenerator(name="uuid", strategy= "uuid2")
	private String id;
	private String code;
	private String name;
	private BigDecimal amount;
	private Boolean outOfStock;
}
